export class DataService {
    listItems: Array<object>;

    constructor() {
        /*this.listItems = [{
            itemName: "item 1",
            itemDesc: "item desc 1"
        },{
            itemName: "item 2",
            itemDesc: "item desc 2"
        }];*/

        this.loadListItems();

        console.log(this.listItems);
    }

    get listItemData() {
        return this.listItems;
    }

    getSingleItem(index: number) {
        console.log(this.listItems[index]);
        return this.listItems[index];
    }

    addListItem(newItemName: string, newItemDesc: string) {
        this.listItems.push({itemName: newItemName, itemDesc: newItemDesc});
        this.saveListItems();        
    }

    removeListItem(index: number) {
        this.listItems.splice(index, 1);
        this.saveListItems();
    }

    editListItemName(index: number, newItemName: string) {
        this.listItems[index].itemName = newItemName;
        this.saveListItems();
    }

    editListItemDesc(index: number, newItemDesc: string) {
        this.listItems[index].itemDesc = newItemDesc;
        this.saveListItems();
    }

    loadListItems() {
        this.listItems = JSON.parse(localStorage.getItem("aeListItems"));

        if(!this.listItems) {
            this.listItems = [{
                itemName: "item 1",
                itemDesc: "item desc 1"
            },{
                itemName: "item 2",
                itemDesc: "item desc 2"
            }];
        }
    }

    saveListItems() {
        localStorage.setItem("aeListItems", JSON.stringify(this.listItems));
    }
}