import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ListItem } from '../listItem/listItem';
import { DataService } from '../../services/dataService/dataService';

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})

export class ListPage {
    itemList: any;

    constructor(public navCtrl: NavController, public navPrms: NavParams, public alertCtrl: AlertController, public dataServ: DataService) {
        this.itemList = dataServ.listItemData;
    }

    viewDetails(index: number) {
        console.log(index);

        this.navCtrl.push(ListItem, {
            itemIndex: index
        });
    }

    deleteItem(index: number, $event) {
        $event.stopPropagation();

        let confirmDialog = this.alertCtrl.create({
            title: "Delete?",
            message: "Are you sure you want to delete this item?",
            buttons: [{
                text: "No",
                role: "cancel"
            },{
                text: "Yes",
                handler: () => {
                    this.dataServ.removeListItem(index);
                }
            }]
        });

        confirmDialog.present();
        return;
    }

    addNewItem() {
        this.navCtrl.push(ListItem, {
            itemIndex: null
        });
    }
}