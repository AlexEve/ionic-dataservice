import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DataService } from '../../services/dataService/dataService';

@Component({
    selector: 'page-listItem',
    templateUrl: 'listItem.html'
})

export class ListItem {
    itemIndex: number;
    itemName: string;
    itemDesc: string;
    itemIsNew: boolean;

    // ngModel input fields
    itemNameInput: string;
    itemDescInput: string;

    constructor(public navCtrl: NavController, public navPrms: NavParams, public alertCtrl: AlertController, public dataServ: DataService) {
        this.itemIndex = navPrms.get('itemIndex');

        // Populate input fields if data exists
        if(this.itemIndex !== null) {
            let itemData = dataServ.getSingleItem(this.itemIndex);

            console.log(itemData);

            this.itemName = itemData.itemName;
            this.itemDesc = itemData.itemDesc;

            this.itemIsNew = false;
        } else {
            this.itemName = "";
            this.itemDesc = "";

            this.itemIsNew = true;
        }

        console.log("Item is new? " + this.itemIsNew);
    }

    saveItem() {
        console.log(this.itemNameInput);
        console.log(this.itemDescInput);

        if(this.itemNameInput !== undefined) {
            if(this.itemIsNew) {
                // Add new list item
                this.dataServ.addListItem(this.itemNameInput, this.itemDescInput);
            } else {
                // Edit existing list item if needed
                if(this.itemNameInput !== this.itemName) {
                    this.dataServ.editListItemName(this.itemIndex, this.itemNameInput);
                }
                if(this.itemDescInput !== this.itemDesc) {
                    this.dataServ.editListItemDesc(this.itemIndex, this.itemDescInput);
                }
            }

            this.navCtrl.pop();
        } else {
            const alert = this.alertCtrl.create({
                title: 'No Item Name',
                subTitle: 'Please enter a name for your item before saving',
                buttons: ['Ok']
            });

            alert.present();
        }        
    }

    deleteItem() {
        let confirmDialog = this.alertCtrl.create({
            title: "Delete?",
            message: "Are you sure you want to delete this item?",
            buttons: [{
                text: "No",
                role: "cancel"
            },{
                text: "Yes",
                handler: () => {
                    this.dataServ.removeListItem(this.itemIndex);
                    this.navCtrl.pop();
                }
            }]
        });

        confirmDialog.present();
    }
}
